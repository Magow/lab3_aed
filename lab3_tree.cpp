#include <iostream>
#include <cstdlib>

using namespace std;

struct Nodo{

    float prom;
    int nro_matricula;
    struct Nodo* izq;
    struct Nodo* der;
};
// creación de nuevos nodos, empezando por la raíz 
struct Nodo *newNodo(int nro_matricula, float prom){ //, int nro_matricula
    struct Nodo *new_Nodo = (struct Nodo *) malloc(sizeof(struct Nodo));
    new_Nodo->prom = prom;
    new_Nodo->izq = new_Nodo->der = NULL;
    new_Nodo->nro_matricula = nro_matricula;
    return new_Nodo;
}

void branches(struct Nodo *nodo, int nro_matricula, float prom){
    //condiciones de número mayor o menor a la raíz
    // menor o igual -> rama derecha, mayor -> rama izquierda
    
    if (prom > nodo-> prom ){
        if (nodo->izq == NULL){
            nodo->izq = newNodo(nro_matricula, prom);
        }
        else {
            branches(nodo->izq, nro_matricula, prom);
        }
    }
    else {
        if (nodo->der == NULL){
            nodo->der = newNodo(nro_matricula, prom);
        }
        else{
            branches(nodo->der,nro_matricula, prom);
        }
    }
}

void preorden(struct Nodo *nodo) {
    if (nodo != NULL) {
        cout << "Alumno número: " << nodo->nro_matricula<< endl;
        cout << "promedio :" << nodo->prom << endl; 
        preorden(nodo->izq);
        preorden(nodo->der);
    }
}

void postorden(struct Nodo *nodo) {
    if (nodo != NULL) {
        postorden(nodo->izq);
        postorden(nodo->der);
        cout << "Alumno número: " << nodo->nro_matricula<< endl;
        cout << "promedio :" << nodo->prom << endl; 
    }
}

void inorden(struct Nodo *nodo) {
    if (nodo != NULL) {
        inorden(nodo->izq);
        cout << "Alumno número: " << nodo->nro_matricula<< endl;
        cout << "promedio :" << nodo->prom << endl; 
        inorden(nodo->der);
    }
}



int main()
{
    int opcion;
    bool funciona = true;
    
    int nro_matricula = 0;
    float prom_alumno = 0;

    // iniciar el arbol (raiz)
    cout << "Para iniciar el arbol debe ingresar un alumno" << endl;
    cout << "Ingrese el número de matrícula del estudiante" << endl;
    cin >> nro_matricula;
    cout << "Ingrese el número de promedio del estudiante" << endl;
    cin >> prom_alumno;
    struct Nodo *raiz = newNodo(nro_matricula, prom_alumno);


    do {
    
        
        // Texto del menú que se verá cada vez
        cout << "\n\nMenu de Opciones" << endl;
        cout << "1. Opcion 1: Ingresar datos de alumno " << endl;
        cout << "2. Opcion 2: Mostrar valores por recorrido pre-orden" << endl;
        cout << "3. Opcion 3: Mostrar valores por recorrido post-orden" << endl;
        cout << "4. Opcion 4: Mostrar valores por recorrido in-orden" << endl;
        cout << "5. Opcion 5: Eliminación de un nodo o rama" << endl;
        cout << "0. SALIR" << endl;
        
        cout << "\nIngrese una opcion: ";
        cin >> opcion;
        
        switch (opcion) {
            case 1:
                // Ingresar nuevos datos al arbol de búsqueda
                cout << "Ingrese el numero de matrícula del estudiante que desee agregar al árbol" << endl;
                cin >> nro_matricula;
                cout << "Ahora ingrese el promedio de notas que tiene el estudiante ingresado " << endl;
                cout << "Separe los decimales con un punto (.) " << endl;
                cin >> prom_alumno;
                // ver si el promedio puede existir en la escala de evaluaciones de la universidad 
                if (prom_alumno > 7.0){
                    cout << "No es posible tener ese promedio";
                    cout << "Se ha impedido el ingreso del alumno";
                    break;
                }
                else {
                    branches(raiz, nro_matricula, prom_alumno);
                    cout << "El alumno ha sido ingresado de manera exitosa" << endl;
                }
                break;
                
            case 2:
                // Mostrar valores por recorrido pre-orden
                cout << "Impresión de datos recorrido pre_orden" << endl;
                preorden(raiz);

                break;
                
            case 3:
                // Mostrar valores por recorrido post-orden
                cout << "Impresión de datos recorrido post-orden" << endl;
                postorden(raiz);
                break;
                
            case 4:
                // Mostrar valores por recorrido in-orden
                cout << "Impresión de datos recorrido in-orden" << endl;
                inorden(raiz);
                break;
            
            
            case 5:
                // Eliminación de un nodo o rama
                
                break;

            case 0:
            	funciona = false;
            	break;
        }
        
    } while (funciona);

    return 0;
}